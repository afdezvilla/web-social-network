angular.module "fakebook-app"
.factory "entryResource", ["$resource", "paths", "headers", ($resource, paths, headers) ->
  $resource paths.api.users.entries.index+"/:id", {id_user:"@user_id", id:"@id"}, {
    update:
      method: 'PUT'
    get:
      headers: headers
    query:
      headers: headers
      isArray: yes
    save:
      method: 'POST'
      headers: headers
  }
]
