angular.module "fakebook-app"

.controller "entryIndexCtrl", ["$scope", "paths", "$http", "headers", ($scope, paths, $http, headers) ->
  $scope.toggle_like = (entry) ->
    url = paths.api.users.entries.toggle_like
    .replace ":id_user", entry.user_id
    .replace ":id", entry.id
    .replace ":id_liker", $scope.current_user.id
    # PUT /api/users/:user_id/entries/:id/like/:id_liker
    $http.put(url, headers:headers)
    .then (result) ->
      entry.liked = not entry.liked
      if entry.liked
        entry.likers_count++
      else
        entry.likers_count--
]

.controller "entryCreateCtrl", ["$scope", "entryResource", "alertsManager", "$route", "Upload", "paths",
($scope, entryResource, alertsManager, $route, Upload, paths) ->
  $scope.post = ->
    data = entry:
      title: $scope.title
      message: $scope.message
    data.entry.image = $scope.file if $scope.file?

    url = paths.api.users.entries.index.replace ":id_user", $scope.current_user.id

    Upload.upload({
      url: url,
      data: data
    }).then (resp) ->
      $('#modal-publish').modal 'hide'
      $route.reload()
      $scope.title = $scope.message = ""
      alertsManager.add "info", "Your entry was publish successfully"

]

.controller "entryShowCtrl", ["$scope","entryResource","$routeParams", "paths", "$http", "headers",
($scope, entryResource,$routeParams, paths, $http, headers) ->
  entryResource.get {id_user: $routeParams.id_user, id: $routeParams.id}, (entry) ->
    $scope.entry = entry

]

.controller "entryDeleteCtrl", ["$scope", "entryResource", "$route", "alertsManager",
 ($scope, entryResource, $route, alertsManager) ->
  $scope.delete = ->
    params =
      id: $scope.entry.id
      id_user: $scope.entry.user_id

    entryResource.delete params, ->
      alertsManager.add "info", "Entry was removed!"
      $route.reload()

]
