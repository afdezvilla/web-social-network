angular.module "fakebook-app"

.filter "mentionsFilter", ["paths", (paths) ->
  (text) ->
    # NOTE text need to be sanitized, use "ng-html-bind"
    text.replace /\B@([a-zA-Z0-9_-]+)\b/g, (match, group) ->
      url = paths.users.nick.replace ":nick", group
      "<a href='#{ url }'>#{ match }</a>"
]
