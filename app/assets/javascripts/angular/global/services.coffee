angular.module "fakebook-app"

.factory 'alertsManager', ->
  alerts: {}
  add: (type, message) ->
    @alerts[type] = @alerts[type] or []
    if message not in @alerts[type]
      @alerts[type].push message

  # NOTE: colection -> {field1:[message1, message2, ...], field2:[...], ...}
  add_colection: (type, colection) ->
    for field of colection
      for message in colection[field]
        @add(type, "#{field}: #{message}")

  remove: (type, message) ->
    if @alerts[type]
      index = @alerts[type].indexOf message
      @alerts[type].splice(index, 1) if index >= 0

  clear: ->
    # NOTE: We can't just replace the object by {},
    # otherwise the scope reference persists.
    delete @alerts[x] for x of @alerts


.factory "userStorage", ["localStorageService", (storage) ->
  user = storage.get("current_user") || {}
  {
    user: (new_val) ->
      return user if not new_val?
      # NOTE: We can't just replace the object by {},
      # otherwise the scope old reference persists.
      delete user[x] for x of user
      for field of new_val
        user[field] = new_val[field]
      storage.set 'current_user', user
    logged: ->
      !!Object.keys(user).length
    save: ->
      storage.set 'current_user', user
    admin: ->
      user.admin
  }
]
