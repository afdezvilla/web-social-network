angular.module "fakebook-app"

.controller "generalCtrl", ["$scope", "localStorageService", "paths", "userStorage"
($scope, storage, paths, userStorage) ->
  $scope.current_user = userStorage.user()
  $scope.logged = userStorage.logged
  $scope.admin = userStorage.admin
  $scope.paths = paths
]

.controller "searchCtrl", ["$scope","$http", "$window", "paths", "$location", "headers", "alertsManager"
($scope, $http, $window, paths, $location, headers, alertsManager) ->
  $scope.users = []

  $scope.autocomplete = ->
    return if not $scope.term
    config =
      params: {limit: 7}
      headers: headers

    url = paths.api.users.by_term.replace(":term", encodeURIComponent($scope.term))
    # GET /api/users/term/:term
    $http.get(url, config)
    .then (result) ->
      $scope.users = ("@#{ user.nick }" for user in result.data)
    , (result) ->
      alertsManager.add "error", result.errors

  $scope.search = ($event) ->
    $event.preventDefault()
    return if not $scope.term
    nick = encodeURIComponent $scope.term.replace("@", "")
    url = paths.users.nick.replace ":nick", nick
    $scope.term = ""
    $location.path url
]

.controller "authCtrl", ["$scope", "Auth", "$location", "paths", "headers", "alertsManager", "userStorage"
($scope, Auth, $location, paths, headers, alertsManager, userStorage) ->
  $scope.sign_in = ->
    credentials =
      password: $scope.password
      email: $scope.email

    Auth.login credentials, headers:headers
    .then (user) ->
        userStorage.user user
        $location.path paths.root
        alertsManager.add "info", "Successfully signed in."
    , (resp) ->
      if resp.data.error
        alertsManager.add "error", resp.data.error
      else
        alertsManager.add_colection "error", resp.data.errors
      $scope.password = $scope.email = ""

  $scope.sign_up = ->
    date =  Date.parse($scope.birthday)
    unless date
      alertsManager.add "error", "Birthday is wrong"
      return

    credentials =
      name: $scope.name
      email: $scope.email
      birthday: (new Date date).toISOString()
      nick: $scope.nick
      password: $scope.password
      password_confirmation: $scope.password_confirmation

    Auth.register credentials, headers:headers
    .then (user) ->
      $location.path paths.root
      alertsManager.add "info", "Successfully signed up, please confirm through your email."
    , (resp) ->
      alertsManager.add_colection "error", resp.data.errors

  $scope.sign_out = ->
    Auth.logout headers:headers
    .then (user) ->
      userStorage.user {}
      $location.path paths.root
      alertsManager.add "info", "Successfully logged out."
    , (resp) ->
      alertsManager.add_colection "error", resp.data.errors
]

.controller "alertsCtrl", ["alertsManager", "$scope", (alertsManager, $scope) ->
  $scope.alerts = alertsManager.alerts
  $scope.remove = alertsManager.remove
]

.controller "welcomeCtrl", ["$scope", "userStorage", "entryResource",
($scope, userStorage, entryResource) ->
  # NOTE: No feed for admins and not logged people
  return if not userStorage.logged() or userStorage.admin()
  data =
    id_user: $scope.current_user.id
    feed: yes
  entryResource.query data, (entries) ->
    $scope.entries = entries

]

.controller "statusesCtrl", ["$scope", ($scope) ->

]
