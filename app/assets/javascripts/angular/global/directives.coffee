angular.module "fakebook-app"
.directive "usersAutocomplete", ->
  link: (scope, element, attrs) ->
    $(element).autocomplete
      source: scope.users
      select: (ev, ui) ->
        scope.term = ui.item.value
        scope.$apply()
      search: ->
        $(@).autocomplete "option", "source", scope.users
      create: ->
        $('.ui-helper-hidden-accessible').remove()
