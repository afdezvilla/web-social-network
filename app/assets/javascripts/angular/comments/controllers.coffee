angular.module "fakebook-app"

.controller "commentIndexCtrl", ["$scope", "commentResource", "$routeParams",
($scope, commentResource, $routeParams) ->
	# GET /api/users/:user_id/entry/:id
    commentResource.query {id_user: $routeParams.id_user, id_entry: $routeParams.id}, (comments) ->
    	$scope.comments = comments
]

.controller "commentCreateCtrl", ["$scope", "commentResource", "alertsManager", "$route", "Upload", "paths", "$routeParams", ($scope, commentResource, alertsManager, $route, Upload, paths, $routeParams) ->
  $scope.post = ->
    comment =
      content: $scope.message
      id_user: $routeParams.id_user
      id_entry: $routeParams.id
    # POST /users/:user_id/entries/:entry_id/comments
    commentResource.save(comment).$promise
    .then ->
      $('#modal-comments').modal "hide"
      $('body').removeClass('modal-open')
      $('.modal-backdrop').remove()
      $route.reload()
      $scope.message = ""
      alertsManager.add "info", "Your comment was publish successfully"




]
