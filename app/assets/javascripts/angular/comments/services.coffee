angular.module "fakebook-app"
.factory "commentResource", ["$resource", "paths", "headers", ($resource, paths, headers) ->
  $resource paths.api.users.entries.comments.index+"/:id", {id_user:"@id_user", id_entry:"@id_entry", id:"@id"}, {
    update:
      method: 'PUT'
    get:
      headers: headers
    query:
      headers: headers
      isArray: yes
    save:
      method: 'POST'
      headers: headers
  }
]
