json.array!(@entries) do |entry|
  json.extract! entry, :id, :message, :user_id, :title, :created_at, :updated_at, :likers_count
  json.user_nick entry.user.nick
  json.image_url entry.image.url if entry.image.url != ""
  json.user_avatar_url entry.user.avatar.url
  json.liked entry.liked_by? current_user
  json.url user_entry_url(entry.user, entry, format: :json)
end
