json.array!(@comments) do |comment|
  json.extract! comment, :id, :content, :user_id, :created_at
  json.user_avatar_url comment.user.avatar.url
  json.user_nick comment.user.nick
end
