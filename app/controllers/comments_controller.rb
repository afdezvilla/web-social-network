class CommentsController < ApplicationController
  before_filter :authenticate_user!, only: [:create]
  before_filter :set_user, only: [:index, :create]
  before_filter :set_entry, only: [:index, :create]
  before_filter :check_chain, only: [:index]
  respond_to :json

  # GET /user/:user_id/entries/:id_entry/comments
  def index
    @comments = @entry.comments
    respond_with @user, @entry, @entry.comments
  end

  # POST /user/:user_id/entries/:id_entry/comments
  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.entry = @entry
    if @comment.save
      render nothing:true, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:content)
    end

    def find_commentable
      @commentable_type = params[:commentable_type].classify
      @commentable = @commentable_type.constantize.find(params[:commentable_id])
    end

    def set_user
      id = (params[:id] or params[:user_id])
      @user = find_user id
    end

    def set_entry
      id = (params[:id] or params[:entry_id])
      @entry = find_entry id
    end

    def find_user id
      if User.exists? id
        User.find id
      else
        render nothing:true, status: :not_found
      end
    end

    def find_entry id
      if Entry.exists? id
        Entry.find id
      else
        render nothing:true, status: :not_found
      end
    end

    def check_chain
      unless @user.entries.member? @entry
        render nothing:true, status: :not_found
      end
    end

end
