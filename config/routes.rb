Rails.application.routes.draw do
  # Root layout
  root to: "application#show"

  scope "api" do
    # Devise
    devise_for :users, controllers: {sessions: 'sessions', registrations: 'registrations', confirmations: "confirmations"}, :path => 'auth', skip: [:registrations, :passwords, :confirmations, :sessions]
    scope "auth" do
      devise_scope :user do
        post "/sign_in", to: "sessions#create"
        delete "/sign_out", to: "sessions#destroy"
        post "/sign_up", to: "registrations#create"
        get "/confirmation", to: "confirmations#show"
      end
    end

    # REST resources
    resources :users, only: [:show, :update, :index] do
      resources :entries, except: [:update, :new, :edit] do
          resources :comments, only: [:index, :create, :destroy]
          put '/toggle_like/:id_liker', to: "entries#like"
      end
      put '/toggle_follow/:id_followable', to: 'users#toggle_follow'
      put "/toggle_ban", to: 'users#toggle_ban'
      put "/make_admin", to: 'users#make_admin'
    end
    get '/users/name/:nick', to: 'users#show_by_nick'
    get '/users/term/:term', to: 'users#index_by_term'
  end

  # Default
  get '*path', to: 'application#show'

end
