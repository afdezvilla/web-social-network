class SocializeCounts < ActiveRecord::Migration
  def change
    add_column :users, :followees_count, :integer, :default => 0
    add_column :users, :followers_count, :integer, :default => 0
    add_column :users, :likees_count, :integer, :default => 0
    add_column :entries, :likers_count, :integer, :default => 0
    add_column :entries, :mentionees, :integer, :default => 0
    add_column :users, :mentioners_count, :integer, :default => 0
  end
end
